#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, makePrediction
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------


class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.3\n3.4\n3.7\n0.92\n")

    def test_eval_2(self):
        r = StringIO("10183:\n642612\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10183:\n3.0\n1.0\n")

    # checking when rating values are the same

    def test_eval_3(self):
        r = StringIO("9246:\n2017834\n738364\n1366191\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "9246:\n4.0\n4.0\n4.0\n1.15\n")

    def test_eval_4(self):
        r = StringIO("9246:\n1366191\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "9246:\n4.0\n0.0\n")

    # checking if same customerID entered twice

    def test_eval_5(self):
        r = StringIO("4579:\n65902\n65902\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "4579:\n3.2\n3.2\n1.2\n")

    # checking for two movies entered at once

    def test_eval_6(self):
        r = StringIO("6682:\n1501175\n7951:\n857963\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "6682:\n3.0\n7951:\n4.0\n0.71\n")
        
    #testing our prediction function 
    
    def test_prediction_1(self):
        movie = 6682
        cust = 1501175
        out = makePrediction(movie,cust)
        self.assertEqual(out, 3.0)
        
    def test_prediction_2(self):
        movie = 9246
        cust = 1366191
        out = makePrediction(movie,cust)
        self.assertEqual(out, 4.0)
        
    def test_prediction_3(self):
        movie = 4579
        cust = 65902
        out = makePrediction(movie,cust)
        self.assertEqual(out, 3.2)
    
# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
