#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, netflix_line_strip, netflix_pred_avg_movie_rating, netflix_pred_avg_customer_rating, netflix_rmse
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.3\n3.2\n3.6\n0.83\n")

    def test_eval_2(self):
        r = StringIO("13816:\n1818146\n89710\n1770937\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "13816:\n4.0\n3.6\n3.9\n0.62\n")

    def test_eval_3(self):
        r = StringIO("13822:\n2079376\n2456463\n395709\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "13822:\n3.8\n3.4\n3.6\n0.76\n")




    def test_line_strip_1(self):
        line = ("13813:\n")
        line = netflix_line_strip(line)
        self.assertEqual(
            line, "13813:")

    def test_line_strip_2(self):
        line = ("66338\n")
        line = netflix_line_strip(line)
        self.assertEqual(
            line, "66338")

    def test_line_strip_3(self):
        line = ("\n1682273\n")
        line = netflix_line_strip(line)
        self.assertEqual(
            line, "1682273")




    def test_pred_avg_movie_rating_1(self):
        line =  ("13824:")
        pred, current_movie = netflix_pred_avg_movie_rating(line)
        self.assertEqual(
            pred, 2.882)
        self.assertEqual(
            current_movie, "13824")

    def test_pred_avg_movie_rating_2(self):
        line =  ("10004:")
        pred, current_movie = netflix_pred_avg_movie_rating(line)
        self.assertEqual(
            pred, 4.209)
        self.assertEqual(
            current_movie, "10004")

    def test_pred_avg_movie_rating_3(self):
        line =  ("1001:")
        pred, current_movie = netflix_pred_avg_movie_rating(line)
        self.assertEqual(
            pred, 3.317)
        self.assertEqual(
            current_movie, "1001")



    def test_pred_avg_customer_rating_1(self):
        line =  ("1035565")
        pred = 2.882
        prediction, current_customer = netflix_pred_avg_customer_rating(line, pred)
        self.assertEqual(
            prediction, 3.2)
        self.assertEqual(
            current_customer, "1035565")

    def test_pred_avg_customer_rating_2(self):
        line =  ("1737087")
        pred = 4.209
        prediction, current_customer = netflix_pred_avg_customer_rating(line, pred)
        self.assertEqual(
            prediction, 4.4)
        self.assertEqual(
            current_customer, "1737087")

    def test_pred_avg_customer_rating_3(self):
        line =  ("1050889")
        pred = 3.317
        prediction, current_customer = netflix_pred_avg_customer_rating(line, pred)
        self.assertEqual(
            prediction, 4.0)
        self.assertEqual(
            current_customer, "1050889")



    def test_rmse_1(self):
        predictions = [3.2, 2.5, 3.6]
        actual= [4, 2, 4]
        rmse = netflix_rmse(predictions, actual)
        self.assertEqual(
            str(rmse)[:4], "0.59")

    def test_rmse_2(self):
        predictions = [2.7, 3.2, 2.2]
        actual= [2, 3, 3]
        rmse = netflix_rmse(predictions, actual)
        self.assertEqual(
            str(rmse)[:4], "0.62")

    def test_rmse_3(self):
        predictions = [4.5, 3.2, 4.3]
        actual= [4, 4, 3]
        rmse = netflix_rmse(predictions, actual)
        self.assertEqual(
            str(rmse)[:4], "0.92")
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
