#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, create_cache
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.0023809999999997\n3.1905150000000004\n3.8466820000000004\n0.90\n")

    def test_eval_2(self):
        r = StringIO("1068:\n962487\n1945809\n808135\n1181281\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual( w.getvalue(), "1068:\n3.766909\n2.851941\n3.4188210000000003\n2.599985\n0.64\n")

    def test_eval_3(self):
        r = StringIO("10721:\n1152639\n386988\n442604\n1490750\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual( w.getvalue(), "10721:\n4.378534\n3.2409539999999994\n2.6828209999999997\n3.5223079999999998\n0.77\n" )

    def test_eval_4(self):
        r = StringIO("10725:\n246107\n209111\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual( w.getvalue(), "10725:\n3.5390040000000003\n3.870941\n0.33\n" )
 
    def test_cache_1(self):
        AVERAGE_MOVIE_RATING_PER_YEAR = create_cache("cache-movieAverageByYear.pickle")
        self.assertEqual(65433, len(AVERAGE_MOVIE_RATING_PER_YEAR))

    def test_cache_2(self):
        CUSTOMER_AVERAGE_RATING_YEARLY = create_cache("cache-customerAverageRatingByYear.pickle")
        self.assertEqual(908179, len(CUSTOMER_AVERAGE_RATING_YEARLY))

    def test_cache_3(self):
        YEAR_OF_RATING = create_cache("cache-yearCustomerRatedMovie.pickle")
        self.assertEqual(1408395, len(YEAR_OF_RATING))

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
