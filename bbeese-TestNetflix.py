#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, create_cache
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract
import pickle

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_cache_1(self):
        self.assertIsNotNone(create_cache("cache-actualCustomerRating.pickle"))

    def test_cache_2(self):
        self.assertIsNotNone(create_cache("cache-movieAverageByYear.pickle"))

    def test_cache_3(self):
        self.assertIsNotNone(create_cache("cache-yearCustomerRatedMovie.pickle"))
	
    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.393096654806667\n3.50842998814\n3.7547633214733334\n0.94\n")

    def test_eval_2(self):
        r = StringIO("6110:\n1957175\n583554\n133059\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "6110:\n3.6997633214733328\n3.708763321473333\n3.4757633214733334\n1.14\n")

    def test_eval_parsing(self):
        r = StringIO("10024:\n311377\n2161379\n1611053\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10024:\n3.6150966548066665\n3.62842998814\n3.4337633214733336\n0.86\n")

    

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
