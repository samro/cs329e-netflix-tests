#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("6739:\n6721\n1792800\n2559262\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "6739:\n4.05071003558\n4.436710035580001\n4.794710035580001\n0.28\n")

    def test_eval_2(self):
        r = StringIO("9913:\n389134\n537789\n2319678")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "9913:\n3.4877100355800006\n4.169710035580001\n3.51271003558\n0.78\n")

    def test_eval_3(self):
        r = StringIO("10:\n1952305\n1531863\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "10:\n2.98571003558\n2.7207100355800002\n0.19\n")

    def test_eval_4(self):
        r = StringIO("9936:\n2612889\n2500699\n202667\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "9936:\n3.70671003558\n2.8147100355800005\n2.95371003558\n0.42\n")
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
