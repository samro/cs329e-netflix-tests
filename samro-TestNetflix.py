#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, netflix_pred
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "1:\n3.8\n3.5\n3.5\n0.42\n")
    def test_eval_2(self):
        r = StringIO("10:\n1952305\n1531863\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "10:\n3.0\n2.7\n0.21\n")

    def test_eval_3(self):
        r = StringIO("10001:\n262828\n2609496\n1474804\n831991\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), "10001:\n3.7\n4.4\n3.9\n2.8\n0.65\n")


    # ----
    # pred
    # ----

    def test_pred_1(self):
        c = 470861
        m = 1
        y = 2004
        pred = netflix_pred(c, m, y)
        self.assertEqual(pred, 4.4)

    def test_pred_2(self):
        c = 2623506
        m = 1000
        y = 2005
        pred = netflix_pred(c, m, y)
        self.assertEqual(pred, 3.1)

    def test_pred_3(self):
        c = 2224061
        m = 10010
        y = 2005
        pred = netflix_pred(c, m, y)
        self.assertEqual(pred, 2.5)
        
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
