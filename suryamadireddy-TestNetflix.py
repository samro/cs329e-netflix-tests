#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract
from unittest import main, TestCase

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), '10040:\n3.28\n3.46\n3.83\n0.94\n')

    def test_eval_2(self):
        r = StringIO("1000:\n2326571\n977808\n1010534\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), '1000:\n3.42\n3.27\n3.15\n0.72\n')

    def test_eval_3(self):
        r = StringIO("10001:\n262828\n2609496\n1474804\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), '10001:\n3.63\n3.98\n3.77\n0.25\n')

    def test_eval_4(self):
        r = StringIO("10037:\n253214\n612895\n769764\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), '10037:\n3.89\n3.36\n3.56\n0.80\n')

    def test_eval_5(self):
        r = StringIO("1004:\n910485\n1970126\n1557423\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), '1004:\n2.96\n2.79\n3.31\n0.21\n')

    def test_eval_6(self):
        r = StringIO("10050:\n578168\n498400\n2540580\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), '10050:\n3.18\n3.11\n3.30\n1.44\n')

# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
