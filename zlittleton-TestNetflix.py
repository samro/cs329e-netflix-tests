#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract


# -----------
# TestNetflix
# -----------

class TestNetflix(TestCase):

    def test_eval_1(self):
        r = StringIO("10002:\n1450941\n1213181\n308502\n2581993\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10002:\n4.0\n3.5\n4.5\n3.8\n0.85\n")

    def test_eval_2(self):
        r = StringIO("10031:\n548690\n2518265\n1575474\n2203195\n1578801\n2614194\n1224756\n2157060\n1528644\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
        w.getvalue(), "10031:\n3.5\n3.2\n3.7\n3.0\n3.7\n3.3\n3.1\n2.9\n3.2\n0.90\n")

    def test_eval_3(self):
        r = StringIO("10037:\n253214\n612895\n769764\n396150\n2555464\n1988133\n560277\n1839686\n2569369\n1845071\n1627613\n948108\n1316833\n911710\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10037:\n3.8\n3.3\n3.4\n3.7\n3.6\n3.7\n3.4\n3.1\n3.5\n3.7\n3.5\n3.4\n3.4\n3.5\n0.71\n")

    def test_eval_4(self): # testing just one prediction, where cAvg is quite higher than mAvg (--> scale factor)
        r = StringIO("10224:\n1712844\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10224:\n4.1\n0.92\n")

    def test_eval_5(self):
        r = StringIO("10161:\n909302\n2291776\n2411892\n1293472\n1663781\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10161:\n3.3\n3.6\n3.6\n5.0\n3.8\n0.41\n")

    def test_eval_6(self): #running same input twice to ensure same results
        r = StringIO("10161:\n909302\n2291776\n2411892\n1293472\n1663781\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10161:\n3.3\n3.6\n3.6\n5.0\n3.8\n0.41\n")

    # (no tests for create_cache since we did not change the default function
    #  and only used given caches)


# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
......
----------------------------------------------------------------------
Ran 6 tests in 0.002s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          50      3     12      2    92%   25-26, 78, 24->25, 77->78
TestNetflix.py      38      0      0      0   100%
------------------------------------------------------------
TOTAL               88      3     12      2    95%

"""
